#include "lv2.h"
#include <iostream>
#include <cmath>


/* class definition */
class TestSynth {
public:
    TestSynth() = delete;
    TestSynth(const TestSynth&) = delete;
    TestSynth(const TestSynth&&) = delete;
    TestSynth(const double srate) {
        this->srate = srate;
    }
    virtual ~TestSynth() {};

    void connect_port(const uint32_t port, void* data) {
        if (port == 0) {
            audio_out = (float*)data;
        }
        if (port == 1) {
            freq = (float*)data;
        }
        if (port == 2) {
            gain = (float*)data;
        }
    }

    void run(const uint32_t sample_count) {
        if (!audio_out || !freq || !gain) return;

        for (uint32_t i = 0; i < sample_count; i++) {
            audio_out[i] = (*gain)*sin(2.0*M_PI*position);
            position += (*freq)/srate;
        }
    }

    void activate() {

    }

    void deactivate() {

    }

private:
    float* audio_out = nullptr;
    float* freq = nullptr;
    float* gain = nullptr;
    double srate = 0.0;
    double position = 0.0;
};


/* internal core methods */
static LV2_Handle instantiate (const struct LV2_Descriptor* descriptor, double sample_rate, const char* bundle_path, const LV2_Feature* const* features) {
    std::cout << "aaaa" << std::endl;
    return new TestSynth(sample_rate);
}


static void connect_port (LV2_Handle instance, uint32_t port, void *data_location) {
    TestSynth* p = (TestSynth*)instance;
    if (!p) return;

    p->connect_port(port, data_location);
}


static void activate (LV2_Handle instance) {
    TestSynth* p = (TestSynth*)instance;
    if (!p) return;

    p->activate();
}


static void deactivate (LV2_Handle instance) {
    TestSynth* p = (TestSynth*)instance;
    if (!p) return;

    p->deactivate();
}


static void run (LV2_Handle instance, uint32_t sample_count) {
    TestSynth* p = (TestSynth*)instance;
    if (!p) return;

    p->run(sample_count);
}


static void cleanup (LV2_Handle instance) {
    TestSynth* p = (TestSynth*)instance;
    if (!p) return;

    delete p;
}


static const void* extension_data (const char* uri) {
    return nullptr;
}


/* descriptor */
static const LV2_Descriptor descriptor = {
    "https://gitlab.com/Fluffeu/lv2/test_synth",
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data
};


/* interface */
LV2_SYMBOL_EXPORT const LV2_Descriptor* lv2_descriptor (uint32_t index) {
    std::cout << "aaaa" << std::endl;
    std::cout << "aaaa" << std::endl;
    if (index == 0) return &descriptor;

    return nullptr;
}