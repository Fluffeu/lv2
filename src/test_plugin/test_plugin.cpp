#include "lv2.h"
#include <iostream>


/* class definition */
class TestPlugin {
public:
    float* audio_in;
    float* audio_out;
    float* gain;
};


/* internal core methods */
static LV2_Handle instantiate (const struct LV2_Descriptor* descriptor, double sample_rate, const char* bundle_path, const LV2_Feature* const* features) {
    return new TestPlugin();
}


static void connect_port (LV2_Handle instance, uint32_t port, void *data_location) {
    TestPlugin* p = (TestPlugin*)instance;
    if (!p) return;

    if (port == 0) p->audio_in = (float*)data_location;
    if (port == 1) p->audio_out = (float*)data_location;
    if (port == 2) p->gain = (float*)data_location;
}


static void activate (LV2_Handle instance) {}
static void deactivate (LV2_Handle instance) {}


static void run (LV2_Handle instance, uint32_t sample_count) {
    TestPlugin* p = (TestPlugin*)instance;
    if (!p || !p->audio_in || !p->audio_out || !p->gain) return;

    for (uint32_t i = 0; i < sample_count; i++) {
        p->audio_out[i] = p->audio_in[i]*(*(p->gain));
    }
}


static void cleanup (LV2_Handle instance) {
    TestPlugin* p = (TestPlugin*)instance;
    if (!p) return;

    delete p;
}


static const void* extension_data (const char* uri) {
    return nullptr;
}


/* descriptor */
static const LV2_Descriptor descriptor = {
    "https://gitlab.com/Fluffeu/lv2",
    instantiate,
    connect_port,
    activate,
    run,
    deactivate,
    cleanup,
    extension_data
};


/* interface */
LV2_SYMBOL_EXPORT const LV2_Descriptor* lv2_descriptor (uint32_t index) {
    std::cout << "testtest123123" << std::endl;
    if (index == 0) return &descriptor;

    return nullptr;
}